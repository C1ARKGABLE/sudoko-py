from pprint import pprint

size = 9
valid = range(1, size + 1)

board = [[{"value": 0, "can": set(valid)} for _ in range(size)] for _ in range(size)]

board[8][8]["can"] -= set([1, 2])

pprint(board)
